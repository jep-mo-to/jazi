import numpy as np
import cv2
from matplotlib import pyplot as plt


def compare(image1, image2):
	img1 = cv2.imread(image1,0)          # queryImage
	img2 = cv2.imread(image2,0) # trainImage

	# Initiate SIFT detector
	orb = cv2.ORB_create()

	# find the keypoints and descriptors with SIFT
	kp1, des1 = orb.detectAndCompute(img1,None)
	kp2, des2 = orb.detectAndCompute(img2,None)
	# create BFMatcher object

	bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)
	try:
		# Match descriptors.
		matches = bf.match(des1,des2)
			# Sort them in the order of their distance.
		matches = sorted(matches, key = lambda x:x.distance)
		return float(matches[0].distance)
	except Exception as e:
		return 999999
	

