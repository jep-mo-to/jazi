import math
from sympy import *
x, y, z = symbols ('x y z')
init_printing(use_unicode=True)


# STATIC variables for printing 
COSINE_PRINT = "Please select what you want to find\n1. Degree\n2. Horizontal Axis\n3. Inclined Axis"
SINE_PRINT = "Please select what you want to find\n1. Degree\n2. Vertical Axis\n3. Inclined Axis"
TAN_PRINT = "Please select what you want to find\n1. Degree\n2. Vertical Axis\n3. Horizontal Axis"

HORIZONTAL = "Enter Horizontal Axis:"
INCLINED = "Enter Inclined Axis:"
VERTICAL = "Enter Vertical Axis:"
DEGREES = "Enter Degrees:"

# Dictionaries for conditions
DEGREE_DICT = {
	'1' : {'print1':HORIZONTAL, 'print2':INCLINED, 'formula':'(math.acos(float(axis1)/float(axis2)))*100'}, 
	'2': {'print1':VERTICAL, 'print2':INCLINED, 'formula':'(math.asin(float(axis1)/float(axis2)))*100'}, 
	'3': {'print1':VERTICAL, 'print2':HORIZONTAL, 'formula':'(math.atan(float(axis1)/float(axis2)))*100'},
	'name':'Degree'
}
H_AXIS_DICT = {
	'1': {'print1':DEGREES, 'print2':INCLINED, 'formula':'math.cos(float(axis1)*.01)*float(axis2)'},
	'3': {'print1':DEGREES, 'print2':VERTICAL, 'formula':'float(axis1)/math.tan(float(axis2)*.01)'},
	'name': 'Horizontal Axis'
}
V_AXIS_DICT = {
	'2': {'print1':DEGREES, 'print2':INCLINED, 'formula':'math.sin(float(axis1)*.01)*float(axis2)'},
	'3': {'print1':DEGREES, 'PRINT2':HORIZONTAL, 'formula':'math.tan(float(axis1)*.01)*float(axis2)'},
	'name': 'Vertical Axis'
}
INCLINED_DICT = {
	'1': {'print1':DEGREES, 'print2':HORIZONTAL, 'formula':'float(axis2)/math.cos(float(axis1)*.01)'},
	'2': {'print1':DEGREES, 'print2':VERTICAL, 'formula':'float(axis2)/math.sin(float(axis1)*.01)'},
	'name': 'Inclined Axis'
}

cosine_conditions = {'0': COSINE_PRINT, '1':DEGREE_DICT, '2':H_AXIS_DICT, '3':INCLINED_DICT}
sine_conditions = {'0': SINE_PRINT, '1':DEGREE_DICT, '2':V_AXIS_DICT, '3':INCLINED_DICT}
tan_conditions = {'0': TAN_PRINT, '1':DEGREE_DICT, '2':V_AXIS_DICT, '3':H_AXIS_DICT}
main_conditions = {'1': cosine_conditions, '2': sine_conditions, '3': tan_conditions}

def process(main_choice, dictionary):

	print(dictionary[main_choice]['print1'])
	axis1 = input()
	print(dictionary[main_choice]['print2'])
	axis2 = input()

	result = eval(dictionary[main_choice]['formula'])
	print(dictionary['name'],"is ",result)
	print("==========================================")

def X_and_Y():
	print("Enter 1st equation")
	input1 = input()
	equation1 = list(input1)
	print("Enter 2nd equation")
	input2 = input()
	equation2 = list(input2)

	e1x = ""
	e1y = ""
	e1ans = ""
	for index in range(len(equation1)):
		if equation1[index] == "x":
			for xIndex in range(index):
				e1x = e1x + equation1[xIndex]
		elif equation1[index] == "y":
			yIndex = index-1
			while (yIndex > 1):
				if equation1[yIndex] != "+" and equation1[yIndex] != "/" and equation1[yIndex] != "*":
					e1y = equation1[yIndex]+e1y
					if equation1[yIndex] == "-":
						yIndex = 1
				else:
					yIndex = 1
				yIndex = yIndex-1
		elif equation1[index] == "=":
			ansIndex = index+1
			while (ansIndex < len(equation1)):
				e1ans = e1ans + equation1[ansIndex]
				ansIndex = ansIndex + 1
	e2x = ""
	e2y = ""
	e2ans = ""
	for index in range(len(equation2)):
		if equation2[index] == "x":
			for xIndex in range(index):
				e2x = e2x + equation2[xIndex]
		elif equation2[index] == "y":
			yIndex = index-1
			while (yIndex > 1):
				if equation2[yIndex] != "+" and equation2[yIndex] != "/" and equation2[yIndex] != "*":
					e2y = equation2[yIndex]+e2y
					if equation2[yIndex] == "-":
						yIndex = 1
				else:
					yIndex = 1
				yIndex = yIndex-1
		elif equation2[index] == "=":
			ansIndex = index+1
			while (ansIndex < len(equation2)):
				e2ans = e2ans + equation2[ansIndex]
				ansIndex = ansIndex + 1

	print(str(e2x))

	e1x2 = float(e1x)
	e1y2 = float(e1y)
	e1ans2 = float(e1ans)

	e2x2 = float(e2x)
	e2y2 = float(e2y)
	e2ans2 = float(e2ans)

	e1x = e1x2
	e1y = e1y2
	e1ans = e1ans2
	e2x = e2x2
	e2y = e2y2
	e2ans = e2ans2

	y = 0
	x = 0
	if e1x+e2x == 0:
		y = (e1ans+e2ans)/(e1y+e2y)
	elif e1y+e2y == 0:
		x = (e1ans+e2ans)/(e1x+e2x)

	if e1x > 0 and e2x > 0:
		negative = e2x*-1
		e1yTemp = negative*e1y
		e1ansTemp = negative*e1ans

		e2yTemp = e1x*e2y
		e2ansTemp = e1x*e2ans

		yTemp = e1yTemp+e2yTemp
		ansTemp = e1ansTemp + e2ansTemp

		if yTemp != 1:
			y = ansTemp/yTemp
		else:
			y = ansTemp

while (True):

	print ("Please select from the following:\n1. Cosine\n2. Sine\n3. Tangent\n4. X and Y\n5. Torque\n6. Kinetic Energy\n7. Derivative of Cosine\n8. Integral of Sine")
	
	main_choice = input()

	if main_choice == '4':
		X_and_Y()
	elif any([main_choice == '1', main_choice == '2', main_choice == '3']):
		print(main_conditions[main_choice]['0'])
		second_choice = input()

		process(main_choice, main_conditions[main_choice][second_choice])
